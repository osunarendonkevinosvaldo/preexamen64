/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preexamen64;

/**
 *
 * @author hp
 */
public abstract class NotaVenta {
    protected int numVenta;
    protected String fecha;
    protected String concepto;
    protected Productos productoPere;
    protected int cantidad;
    protected int tipoPago;

    public NotaVenta() {
        this.numVenta = 0;
        this.fecha = "";
        this.concepto = "";
        this.productoPere = new ProductoPerecedero() ;
        this.cantidad = 0;
        this.tipoPago = 0;
    }

    public NotaVenta(int numVenta, String fecha, String concepto, Productos productoPere, int cantidad, int tipoPago) {
        this.numVenta = numVenta;
        this.fecha = fecha;
        this.concepto = concepto;
        this.productoPere = productoPere;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
    }

    public int getNumVenta() {
        return numVenta;
    }

    public void setNumVenta(int numVenta) {
        this.numVenta = numVenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Productos getProductoPere() {
        return productoPere;
    }

    public void setProductoPere(Productos productoPere) {
        this.productoPere = productoPere;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }
    
   
    public  abstract float CalcularPago();
    
    
    
    
}
