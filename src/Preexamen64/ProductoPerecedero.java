/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preexamen64;

/**
 *
 * @author hp
 */
public class ProductoPerecedero extends Productos {
    
    private String fechaCaducidad;
    private float temperatura;

    public ProductoPerecedero() {
        this.fechaCaducidad = "";
        this.temperatura = 0.0f;
    }


    public ProductoPerecedero(String fechaCaducidad, float temperatura, int idProducto, String nomProducto, int uniProduto, float precioUnitario) {
        super(idProducto, nomProducto, uniProduto, precioUnitario);
        this.fechaCaducidad = fechaCaducidad;
        this.temperatura = temperatura;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }
    
    
    
    
    
    
    
    

    @Override
    public float CalcularPrecio() {
        float precio = 0.0f;
        switch (this.getUniProduto()) {
            case 1:
                precio = this.precioUnitario * 1.03f;
                precio = precio * 1.5f;
                break;
            case 2:
                precio = this.precioUnitario * 1.05f;
                precio = precio * 1.5f;
                break;
            case 3:
                precio = this.precioUnitario * 1.04f;
                precio = precio * 1.5f;
                break;
        }
      
        
        return precio;
    }
    
}
