/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preexamen64;

/**
 *
 * @author hp
 */
public class Factura extends NotaVenta implements Iva {

    private String rfc;
    private String nombreCli;
    private String domFiscal;

    public Factura() {
        this.rfc = "";
        this.nombreCli = "";
        this.domFiscal = "";

    }

    public Factura(String rfc, String nombreCli, String domFiscal, int numVenta, String fecha, String concepto, ProductoPerecedero productoPere, int cantidad, int tipoPago) {
        super(numVenta, fecha, concepto, productoPere, cantidad, tipoPago);
        this.rfc = rfc;
        this.nombreCli = nombreCli;
        this.domFiscal = domFiscal;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getDomFiscal() {
        return domFiscal;
    }

    public void setDomFiscal(String domFiscal) {
        this.domFiscal = domFiscal;
    }

    @Override
    public float CalcularPago() {
        
        return this.productoPere.CalcularPrecio()* this.cantidad;
        

    }

    @Override
    public float calcularIva() {
        return this.CalcularPago() * 0.16f;
    }

    

}
