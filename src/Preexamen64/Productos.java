/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preexamen64;

/**
 *
 * @author hp
 */
public abstract class Productos {
    protected int idProducto;
    protected String nomProducto;
    protected int uniProduto;
    protected float precioUnitario;
    
    
    public Productos() {
        this.idProducto = 0;
        this.nomProducto = "";
        this.uniProduto = 0;
        this.precioUnitario = 0.0f;
        
    }

    public Productos(int idProducto, String nomProducto, int uniProduto, float precioUnitario) {
        this.idProducto = idProducto;
        this.nomProducto = nomProducto;
        this.uniProduto = uniProduto;
        this.precioUnitario = precioUnitario;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNomProducto() {
        return nomProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }

    public int getUniProduto() {
        return uniProduto;
    }

    public void setUniProduto(int uniProduto) {
        this.uniProduto = uniProduto;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
   
    public  abstract float CalcularPrecio();
     
    

   
    
    
}
