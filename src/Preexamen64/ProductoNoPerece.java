/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preexamen64;

/**
 *
 * @author hp
 */
public class ProductoNoPerece extends Productos {
    
    private String loteFabricacion;

    public ProductoNoPerece() {
        this.loteFabricacion = "";
    }

    public ProductoNoPerece(String loteFabricacion, int idProducto, String nomProducto, int uniProduto, float precioUnitario) {
        super(idProducto, nomProducto, uniProduto, precioUnitario);
        this.loteFabricacion = loteFabricacion;
    }

    public String getLoteFabricacion() {
        return loteFabricacion;
    }

    public void setLoteFabricacion(String loteFabricacion) {
        this.loteFabricacion = loteFabricacion;
    }
    
        
    @Override
    public float CalcularPrecio() {
        
        return this.precioUnitario*1.5f;
       
    }
    
}
